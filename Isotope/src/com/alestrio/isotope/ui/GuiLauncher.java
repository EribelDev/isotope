/*
 * Copyright (c) 2018.
 * Code written by Alexis LEBEL (aka Alestrio)
 *
 */

package com.alestrio.isotope.ui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public
class GuiLauncher extends Application {

    public static
    void main (String[] args) {
        Application.launch(args);
    }

    @Override
    public
    void start (Stage primaryStage) throws Exception {
        primaryStage.setTitle("Isotope H");
        Parent root  = FXMLLoader.load(getClass().getResource("fxml/IsotopeLauncher.fxml"));
        Scene  scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}